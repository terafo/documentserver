unit UtilUnit;

interface

uses
   System.Classes;

function getStringFromString(s, sDeliminator: string; iDelNumberFrom,iDelNumberTo: integer): string;
//function MD5(fs: TFileStream) : string;
function CheckIfFileIsEqual(FileName1, FileName2: string): boolean;

implementation

uses
  IdHashMessageDigest, System.SysUtils;


function getStringFromString(s, sDeliminator: string;
  iDelNumberFrom, iDelNumberTo: integer): string;
var sNew : string;
  i,ii : integer;
begin
  for i := 0 to iDelNumberFrom -1 do
  begin
    s := copy(s,pos(sDeliminator,s)+1,length(s));
  end;

  if iDelNumberTo > 0 then
  begin
    sNew := '';
    for i := iDelNumberFrom to iDelNumberTo-1 do
    begin
      sNew := sNew + copy(s,1,pos(sDeliminator,s));
      s := copy(s,pos(sDeliminator,s)+1,length(s));
    end;
    result := copy(sNew,1,length(sNew)-1);
  end
  else
    result := s;
end;

function MD5(fs: TFileStream) : string;
var
   idmd5 : TIdHashMessageDigest5;
   //hash : T4x4LongWordRecord;
begin
   idmd5 := TIdHashMessageDigest5.Create;
   try
     result := idmd5.HashStreamAsHex(fs);
   finally
     idmd5.Free;
   end;
end;

function CheckIfFileIsEqual(FileName1, FileName2: string): boolean;
var
    fs1, fs2: TFileStream;
    fs1Result, fs2Result: string;
begin
    result:=false;
    fs1:=TFileStream.Create(FileName1,fmShareDenyNone);
    try
        fs2:=TFileStream.Create(FileName2, fmShareDenyNone);
        try
            fs1Result:=MD5(fs1);
            fs2Result:=MD5(fs2);
        finally
            fs2.Free;
        end;
    finally
        fs1.Free;
    end;

    result:= CompareStr(fs1Result,fs2Result) = 0;
end;




end.
