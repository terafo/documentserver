unit dmDocumentsUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.UI.Intf,
  FireDAC.VCLUI.Wait, FireDAC.Stan.StorageBin, FireDAC.Stan.StorageJSON,
  FireDAC.Comp.UI, Data.FireDACJSONReflect, IdBaseComponent, IdComponent,
  IdCustomTCPServer, IdTCPServer, IdContext, IdHashMessageDigest, idHash,
  dmSystemTablesUnit, UtilUnit;

type
  TCommand = record
    CmdType: string;
    CmdData: string;
  end;

  TdmDocuments = class(TDataModule)
    FDTransDocuments: TFDTransaction;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    FDMemDocument: TFDMemTable;
    FDMemDocumentDOCID: TIntegerField;
    FDMemDocumentFILENAME: TStringField;
    FDMemDocumentDOCUMENT: TBlobField;
    FDQSaveDocument: TFDQuery;
    FDMemDocumentDATAAREAID: TSmallintField;
    FDMemDocumentRELTABLE: TIntegerField;
    FDMemDocumentRELTABLELINEID: TIntegerField;
    FDQGetDocByID: TFDQuery;
    FDQDocRelation: TFDQuery;
    FDQDocRelationLINEID: TIntegerField;
    FDQDocRelationDOCID: TIntegerField;
    FDQDocRelationRELTABLEID: TSmallintField;
    FDQDocRelationRELTABLELINEID: TIntegerField;
    FDQDocVersion: TFDQuery;
    FDQDocVersionDOCID: TIntegerField;
    FDQDocVersionVERSIONID: TIntegerField;
    FDQDocVersionFILEPATH: TStringField;
    FDQDocVersionFILENAME: TStringField;
    FDQDocVersionDATAAREAID: TSmallintField;
    FDQDocVersionCREATEDBY: TStringField;
    FDQDocVersionCREATEDDATE: TSQLTimeStampField;
    FDQGetDocByIDDOCID: TIntegerField;
    FDQGetDocByIDDATAAREAID: TSmallintField;
    FDQGetMaxVersionID: TFDQuery;
    FDQGetMaxVersionIDMAXID: TIntegerField;
    FDMemDocumentEMPLID: TStringField;
    FDTransUpdate: TFDTransaction;
    FDMemDocumentDOCVERSIONID: TIntegerField;
    FDQGetDocByIDVERSIONID: TIntegerField;
    FDQGetDocByIDFILEPATH: TStringField;
    FDQGetDocByIDFILENAME: TStringField;
    FDQGetDocByIDCREATEDBY: TStringField;
    FDQGetDocByIDCREATEDDATE: TSQLTimeStampField;
    FDQGetDocByRelation: TFDQuery;
    FDQGlobalDocList: TFDQuery;
    FDQGlobalDocListDOCID: TIntegerField;
    FDQGlobalDocListDESCRIPTION: TStringField;
    FDQGlobalDocListDATAAREAID: TSmallintField;
    FDQGlobalDocListRELTABLEID: TSmallintField;
    FDQGlobalDocListRELTABLELINEID: TIntegerField;
    FDQGlobalDocListTABLENAME: TStringField;
    FDQGlobalDocListMASTERFORMNAME: TStringField;
    FDMemGlobalDocList: TFDMemTable;
    FDMemGlobalDocListDOCID: TIntegerField;
    FDMemGlobalDocListDESCRIPTION: TStringField;
    FDMemGlobalDocListDATAAREAID: TSmallintField;
    FDMemGlobalDocListRELTABLEID: TIntegerField;
    FDMemGlobalDocListRELTABLELINEID: TIntegerField;
    FDMemGlobalDocListTABLENAME: TStringField;
    FDMemGlobalDocListMASTERFORMNAME: TStringField;
  private
    { Private declarations }
    procedure LogFile(Sender :TObject; Bod: string);
  public
    { Public declarations }
    function GetDocumentList(RefTableID, DataAreaID: SmallInt; ReftableLineID: integer; EmplID: string; ShowAll: Smallint): TFDJSONDataSets;
    function GetGlobalDocList: TFDJSONDataSets;
    function GetDocument(DocID, DocVersion: integer): TFDJSONDataSets;
    function GenerateVersionID(DocID: integer): integer;
    function GetDocVersions(DocID: integer): TFDJSONDataSets;
    procedure SaveNewDocument;
    procedure UpdateDocument;
    procedure InsertNewVersionToDB(DocID, DocVersionID: integer; FileName: string);
    function AddFilePrefix(DocID, VersionID: integer; FileName: string):string;
    function RemoveFilePrefix(FileName: string):string;

  end;

var
  dmDocuments: TdmDocuments;

implementation

uses
  dmDbConnectionUnit, FMX.Dialogs, FormServiceUnit;

const
    sDocumentList = 'DocumentList';
    sDocumentGlobalList = 'GlobalDocumentList';
    sDocumentByID = 'DocumentByID';
    sDocVersions = 'DocumentVersions';

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TdmDocuments }

function TdmDocuments.AddFilePrefix(DocID, VersionID: integer; FileName: string): string;
begin
    result:=intToStr(DocID)+'_'+IntToStr(VersionID)+'_'+FileName;
end;



function TdmDocuments.RemoveFilePrefix(FileName: string): string;
begin
   result:=getStringFromString(FileName,'_',2,0);
end;

function TdmDocuments.GetDocument(DocID, DocVersion: integer): TFDJSONDataSets;
var
    fs: TFileStream;
    FileName: string;
    FilePath: string;
begin
    FDQGetDocByID.Close;
    FDQGetDocByID.ParamByName('DOCID').Value:=DocID;
    FDQGetDocByID.ParamByName('DOCVERSION').Value:=DocVersion;
    FDQGetDocByID.Open;

    if (FDQGetDocByID.RecordCount = 1) then
    begin
        FileName:=ExtractFileName(FDQGetDocByID.FieldByName('FILENAME').AsString);
        FilePath:=IncludeTrailingPathDelimiter(FDQGetDocByID.FieldByName('FILEPATH').AsString);

        fs := TFileStream.Create(FilePath+FileName, fmOpenRead);
        try
            FDMemDocument.Edit;
            FDMemDocumentDOCID.Value:=DocID;
            FDMemDocumentFILENAME.Value:=RemoveFilePrefix(FileName);
            FDMemDocumentDOCUMENT.LoadFromStream(fs);
            FDMemDocumentDOCVERSIONID.Value:=DocVersion;
            FDMemDocumentDATAAREAID.Value:=FDQGetDocByIDDATAAREAID.Value;
            FDMemDocument.Post;

            Result := TFDJSONDataSets.Create;
            TFDJSONDataSetsWriter.ListAdd(result,sDocumentByID,FDMemDocument);
        finally
            fs.Free;
        end;
    end;

    FDQGetDocByID.Close;
end;

function TdmDocuments.GetDocumentList(RefTableID, DataAreaID: SmallInt; ReftableLineID: integer; EmplID: string; ShowAll: Smallint): TFDJSONDataSets;
begin
    if FDTransDocuments.Active then
       FDTransDocuments.Commit;

    FDQGlobalDocList.Close;
    FDQGlobalDocList.ParamByName('RELTABLEID').Value:=RefTableID;
    FDQGlobalDocList.ParamByName('RELTABLELINEID').Value:=ReftableLineID;
    FDQGlobalDocList.ParamByName('DATAAREAID').Value:=DataAreaID;
    FDQGlobalDocList.ParamByName('SHOWALL').Value:=ShowAll;  // ShowAll = 1 ella 0
    FDQGlobalDocList.Open;
    FDMemGlobalDocList.EmptyDataSet;
    //FDMemGlobalDocList.Close;

    while not FDQGlobalDocList.eof do
    begin
        FDMemGlobalDocList.Insert;
        FDMemGlobalDocListDOCID.Value:=FDQGlobalDocListDOCID.Value;
        FDMemGlobalDocListDESCRIPTION.Value:=FDQGlobalDocListDESCRIPTION.Value;
        FDMemGlobalDocListDATAAREAID.Value:=FDQGlobalDocListDATAAREAID.Value;
        FDMemGlobalDocListRELTABLEID.Value:=FDQGlobalDocListRELTABLEID.Value;
        FDMemGlobalDocListRELTABLELINEID.Value:=FDQGlobalDocListRELTABLELINEID.Value;
        FDMemGlobalDocListTABLENAME.Value:=FDQGlobalDocListTABLENAME.Value;
        FDMemGlobalDocListMASTERFORMNAME.Value:=FDQGlobalDocListMASTERFORMNAME.Value;
        FDMemGlobalDocList.Post;

        FDQGlobalDocList.Next;
    end;

    Result := TFDJSONDataSets.Create;
    TFDJSONDataSetsWriter.ListAdd(result,sDocumentGlobalList,FDMemGlobalDocList);
    FDQGlobalDocList.Close;
end;





function TdmDocuments.GetDocVersions(DocID: integer): TFDJSONDataSets;
begin
    FDQDocVersion.Close;
    FDQDocVersion.ParamByName('DOCID').Value:=DocID;
    FDQDocVersion.Open;
    Result := TFDJSONDataSets.Create;
    TFDJSONDataSetsWriter.ListAdd(result,sDocVersions,FDQDocVersion);
end;

function TdmDocuments.GetGlobalDocList: TFDJSONDataSets;
begin
    if FDTransDocuments.Active then
       FDTransDocuments.Commit;

    FDQGlobalDocList.Close;
    FDQGlobalDocList.ParamByName('RELTABLEID').Value:=-1;
    FDQGlobalDocList.ParamByName('RELTABLELINEID').Value:=-1;
    FDQGlobalDocList.ParamByName('DATAAREAID').Value:=1;
    FDQGlobalDocList.ParamByName('SHOWALL').Value:=1;  // ShowAll = 1 ella 0
    FDQGlobalDocList.Open;
    FDMemGlobalDocList.EmptyDataSet;
    //FDMemGlobalDocList.Close;

    while not FDQGlobalDocList.eof do
    begin
        FDMemGlobalDocList.Insert;
        FDMemGlobalDocListDOCID.Value:=FDQGlobalDocListDOCID.Value;
        FDMemGlobalDocListDESCRIPTION.Value:=FDQGlobalDocListDESCRIPTION.Value;
        FDMemGlobalDocListDATAAREAID.Value:=FDQGlobalDocListDATAAREAID.Value;
        FDMemGlobalDocListRELTABLEID.Value:=FDQGlobalDocListRELTABLEID.Value;
        FDMemGlobalDocListRELTABLELINEID.Value:=FDQGlobalDocListRELTABLELINEID.Value;
        FDMemGlobalDocListTABLENAME.Value:=FDQGlobalDocListTABLENAME.Value;
        FDMemGlobalDocListMASTERFORMNAME.Value:=FDQGlobalDocListMASTERFORMNAME.Value;
        FDMemGlobalDocList.Post;

        FDQGlobalDocList.Next;
    end;

    Result := TFDJSONDataSets.Create;
    TFDJSONDataSetsWriter.ListAdd(result,sDocumentGlobalList,FDMemGlobalDocList);
    FDQGlobalDocList.Close;
    //FDMemGlobalDocList.EmptyDataSet;
end;


procedure TdmDocuments.InsertNewVersionToDB(DocID, DocVersionID: integer; FileName: string);
begin
    dmDocuments.FDQDocVersion.Open;
    dmDocuments.FDQDocVersion.Insert;
    dmDocuments.FDQDocVersionDOCID.Value:=DocID;
    dmDocuments.FDQDocVersionVERSIONID.Value:=DocVersionID;
    dmDocuments.FDQDocVersionFILEPATH.Value:=dmDbConnection.Settings.RootFolder;

    dmDocuments.FDQDocVersionFILENAME.Value:=FileName;
    dmDocuments.FDQDocVersionDATAAREAID.Value:=FDMemDocumentDATAAREAID.Value;
    dmDocuments.FDQDocVersionCREATEDBY.Value:=FDMemDocumentEMPLID.Value;
    dmDocuments.FDQDocVersionCREATEDDATE.AsDateTime:=Now;
    dmDocuments.FDQDocVersion.Post;

    dmDocuments.FDQDocRelation.Open;
    dmDocuments.FDQDocRelation.Insert;
    dmDocuments.FDQDocRelationLINEID.Value:=dmDbConnection.GetGenID('GEN_DOCRELLINEID');
    dmDocuments.FDQDocRelationDOCID.Value:=dmDocuments.FDQGetDocByIDDOCID.Value;
    dmDocuments.FDQDocRelationRELTABLEID.Value:=dmDocuments.FDMemDocumentRELTABLE.Value;
    dmDocuments.FDQDocRelationRELTABLELINEID.Value:=dmDocuments.FDMemDocumentRELTABLELINEID.Value;
    dmDocuments.FDQDocRelation.Post;
end;

procedure TdmDocuments.LogFile(Sender: TObject; Bod: string);
var
  FilNavn: string;
  LogFilur: TextFile;
begin
    // Prepare logfile
    FilNavn:= 'Test.log'; //ChangeFileExt(Application.exename, '.log');
    AssignFile(LogFilur, FilNavn);

    if FileExists(FilNavn) then
       Append(LogFilur)
    else
       Rewrite(LogFilur);

    if Sender is TComponent then
       Writeln(LogFilur, DateTimeToStr(now) + ': '+(Sender as TComponent).Name+': '+Bod)
    else
       Writeln(LogFilur, DateTimeToStr(now) + ': '+Bod);

    CloseFile(LogFilur);
end;

function TdmDocuments.GenerateVersionID(DocID: integer): integer;
begin
    FDQGetMaxVersionID.Close;
    FDQGetMaxVersionID.ParamByName('DOCID').Value:=DocID;
    FDQGetMaxVersionID.Open;

    if FDQGetMaxVersionIDMAXID.IsNull then
       result:=1
    else
       result:=FDQGetMaxVersionIDMAXID.Value+1;
end;

procedure TdmDocuments.SaveNewDocument;
var
  fs, fsOld: TFileStream;
  MD5FileOld, MD5FileNew: string;
  FileName: string;
  DocumentChanged: Boolean;
begin
    DocumentChanged:=True;
    {try
        if FileExists(FileName) then
        begin
           fsOld := TFileStream.Create(FileName, fmOpenRead);
           MD5FileOld:=dmDocuments.MD5(fsOld);
           fs := TFileStream.Create(FileName, fmOpenWrite );
           MD5FileNew:=dmDocuments.MD5(fs);
           DocumentChanged:= (MD5FileOld = MD5FileNew);

           if DocumentChanged then
           begin
              dmDocuments.FDMemDocument.SaveToStream(fs);
              //dmDocuments.SaveNewDocument;
           end;
        end
        else
        begin
           fs := TFileStream.Create(FileName, fmCreate);
           dmDocuments.FDMemDocument.SaveToStream(fs);
           //dmDocuments.SaveNewDocument;
        end;
    finally
        fs.Free;
    end;
    }

    try
        dmDocuments.FDQGlobalDocList.Open;
        dmDocuments.FDQGlobalDocList.Insert;
        dmDocuments.FDQGlobalDocListDOCID.Value:=dmDbConnection.GetGenID('GEN_DOCID');
        dmDocuments.FDQGlobalDocListDATAAREAID.Value:=dmDocuments.FDMemDocumentDATAAREAID.Value;
        dmDocuments.FDQGlobalDocListDESCRIPTION.Value:=FDMemDocumentFILENAME.Value;
        dmDocuments.FDQGlobalDocList.Post;

        dmDocuments.FDQDocVersion.Open;
        dmDocuments.FDQDocVersion.Insert;
        dmDocuments.FDQDocVersionDOCID.Value:=FDQGlobalDocListDOCID.Value;
        dmDocuments.FDQDocVersionVERSIONID.Value:=GenerateVersionID(FDQGlobalDocListDOCID.Value);
        dmDocuments.FDQDocVersionFILEPATH.Value:=dmDbConnection.Settings.RootFolder;

        FileName:=AddFilePrefix(FDQDocVersionDOCID.Value, FDQDocVersionVERSIONID.Value, FDMemDocumentFILENAME.Value);
        FileName:=IncludeTrailingPathDelimiter(dmDbConnection.Settings.RootFolder)+FileName;

        dmDocuments.FDQDocVersionFILENAME.Value:=FileName;
        dmDocuments.FDQDocVersionDATAAREAID.Value:=FDMemDocumentDATAAREAID.Value;
        dmDocuments.FDQDocVersionCREATEDBY.Value:=FDMemDocumentEMPLID.Value;
        dmDocuments.FDQDocVersionCREATEDDATE.AsDateTime:=Now;
        dmDocuments.FDQDocVersion.Post;

        try
            fs := TFileStream.Create(FileName, fmCreate);
            dmDocuments.FDMemDocumentDOCUMENT.SaveToStream(fs);
        finally
            fs.Free;
        end;

        dmDocuments.FDQDocRelation.Open;
        dmDocuments.FDQDocRelation.Insert;
        dmDocuments.FDQDocRelationLINEID.Value:=dmDbConnection.GetGenID('GEN_DOCRELLINEID');
        dmDocuments.FDQDocRelationDOCID.Value:=dmDocuments.FDQGlobalDocListDOCID.Value;
        dmDocuments.FDQDocRelationRELTABLEID.Value:=dmDocuments.FDMemDocumentRELTABLE.Value;
        dmDocuments.FDQDocRelationRELTABLELINEID.Value:=dmDocuments.FDMemDocumentRELTABLELINEID.Value;
        dmDocuments.FDQDocRelation.Post;
    except
        FDTransUpdate.Rollback;
    end;
end;


procedure TdmDocuments.UpdateDocument;
var
  fsNew, fsOld, fsTemp: TFileStream;
  MD5FileOld, MD5FileNew: string;
  FileName, TempFileName: string;
  DocumentChanged: Boolean;
  DocVersionID: integer;
begin
    try
        FDQGetDocByID.Close;
        FDQGetDocByID.ParamByName('DOCID').Value:=FDMemDocumentDOCID.Value;
        FDQGetDocByID.Open;

        if (FDQGetDocByID.RecordCount < 1) then
        begin
            SaveNewDocument;
            Exit;
        end;


        DocVersionID:= FDMemDocumentDOCVERSIONID.Value;
        FileName:=AddFilePrefix(FDMemDocumentDOCID.Value, DocVersionID, FDMemDocumentFILENAME.Value);
        FileName:=IncludeTrailingPathDelimiter(dmDbConnection.Settings.RootFolder)+FileName;

        if FileExists(FileName) then
        begin
           {fsOld := TFileStream.Create(FileName, fmOpenRead);
           try
               MD5FileOld:=dmDocuments.MD5(fsOld);

               //MD5FileNew:=dmDocuments.MD5(fs);
               TempFileName:='tempfile';

               fsTemp := TFileStream.Create(IncludeTrailingPathDelimiter(dmDbConnection.Settings.RootFolder)+TempFileName, fmCreate);
               try
                   FDMemDocumentDOCUMENT.SaveToStream(fsTemp);
               finally
                   fsTemp.Free;
               end;

               fsTemp := TFileStream.Create(IncludeTrailingPathDelimiter(dmDbConnection.Settings.RootFolder)+TempFileName, fmOpenRead);
               try
                   MD5FileNew:=dmDocuments.MD5(fsTemp);
                   DocumentChanged:= (MD5FileOld <> MD5FileNew);
               finally
                   fsTemp.Free;
                   //DeleteFile(IncludeTrailingPathDelimiter(dmDbConnection.Settings.RootFolder)+TempFileName);
               end;

               if DocumentChanged then
               begin
                  DocVersionID:=GenerateVersionID(FDMemDocumentDOCID.Value);
                  FileName:=AddFilePrefix(FDMemDocumentDOCID.Value, DocVersionID, FDMemDocumentFILENAME.Value);
                  FileName:=IncludeTrailingPathDelimiter(dmDbConnection.Settings.RootFolder)+FileName;

                  fsNew:=TFileStream.Create(FileName, fmCreate);
                  try
                      dmDocuments.FDMemDocumentDOCUMENT.SaveToStream(fsNew);
                  finally
                      fsNew.Free;
                  end;
                  InsertNewVersionToDB(FDMemDocumentDOCID.Value, DocversionID, FileName);
               end;
           finally
               fsOld.Free;
           end; }
        end
        else
        begin
           fsNew := TFileStream.Create(FileName, fmCreate);
           try
              dmDocuments.FDMemDocumentDOCUMENT.SaveToStream(fsNew);
              InsertNewVersionToDB(FDMemDocumentDOCID.Value, DocversionID, FileName);
           finally
              fsNew.Free;
           end;
        end;
    except
        FDTransUpdate.Rollback;
    end;
end;

end.
