object dmDocuments: TdmDocuments
  OldCreateOrder = False
  Height = 499
  Width = 735
  object FDTransDocuments: TFDTransaction
    Connection = dmDbConnection.FDConnection1
    Left = 64
    Top = 16
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 488
    Top = 16
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 584
    Top = 16
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 600
    Top = 88
  end
  object FDMemDocument: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'DOCID'
        DataType = ftInteger
      end
      item
        Name = 'FILENAME'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DOCUMENT'
        DataType = ftBlob
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
      end
      item
        Name = 'RELTABLE'
        DataType = ftInteger
      end
      item
        Name = 'RELTABLELINEID'
        DataType = ftInteger
      end
      item
        Name = 'EMPLID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'DOCVERSIONID'
        DataType = ftInteger
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 352
    Top = 208
    object FDMemDocumentDOCID: TIntegerField
      FieldName = 'DOCID'
    end
    object FDMemDocumentFILENAME: TStringField
      FieldName = 'FILENAME'
      Size = 100
    end
    object FDMemDocumentDOCUMENT: TBlobField
      FieldName = 'DOCUMENT'
    end
    object FDMemDocumentDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
    end
    object FDMemDocumentRELTABLE: TIntegerField
      FieldName = 'RELTABLE'
    end
    object FDMemDocumentRELTABLELINEID: TIntegerField
      FieldName = 'RELTABLELINEID'
    end
    object FDMemDocumentEMPLID: TStringField
      FieldName = 'EMPLID'
      Size = 10
    end
    object FDMemDocumentDOCVERSIONID: TIntegerField
      FieldName = 'DOCVERSIONID'
    end
  end
  object FDQSaveDocument: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    SQL.Strings = (
      'select * from documents')
    Left = 352
    Top = 256
  end
  object FDQGetDocByID: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    UpdateTransaction = FDTransUpdate
    SQL.Strings = (
      'select *'
      'from documentversion'
      'where DOCID = :DOCID and VERSIONID = :DOCVERSION')
    Left = 64
    Top = 216
    ParamData = <
      item
        Name = 'DOCID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 9
      end
      item
        Name = 'DOCVERSION'
        DataType = ftWideString
        ParamType = ptInput
        Value = '1'
      end>
    object FDQGetDocByIDDOCID: TIntegerField
      FieldName = 'DOCID'
      Origin = 'DOCID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQGetDocByIDDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQGetDocByIDVERSIONID: TIntegerField
      FieldName = 'VERSIONID'
      Origin = 'VERSIONID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQGetDocByIDFILEPATH: TStringField
      FieldName = 'FILEPATH'
      Origin = 'FILEPATH'
      Required = True
      Size = 256
    end
    object FDQGetDocByIDFILENAME: TStringField
      FieldName = 'FILENAME'
      Origin = 'FILENAME'
      Required = True
      Size = 256
    end
    object FDQGetDocByIDCREATEDBY: TStringField
      FieldName = 'CREATEDBY'
      Origin = 'CREATEDBY'
      Required = True
      Size = 10
    end
    object FDQGetDocByIDCREATEDDATE: TSQLTimeStampField
      FieldName = 'CREATEDDATE'
      Origin = 'CREATEDDATE'
      Required = True
    end
  end
  object FDQDocRelation: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    UpdateTransaction = FDTransUpdate
    SQL.Strings = (
      'select * from docrelation')
    Left = 152
    Top = 216
    object FDQDocRelationLINEID: TIntegerField
      FieldName = 'LINEID'
      Origin = 'LINEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQDocRelationDOCID: TIntegerField
      FieldName = 'DOCID'
      Origin = 'DOCID'
    end
    object FDQDocRelationRELTABLEID: TSmallintField
      FieldName = 'RELTABLEID'
      Origin = 'RELTABLEID'
    end
    object FDQDocRelationRELTABLELINEID: TIntegerField
      FieldName = 'RELTABLELINEID'
      Origin = 'RELTABLELINEID'
    end
  end
  object FDQDocVersion: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    UpdateTransaction = FDTransUpdate
    SQL.Strings = (
      'select * from documentversion'
      'where DOCID = :DOCID')
    Left = 64
    Top = 264
    ParamData = <
      item
        Name = 'DOCID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQDocVersionDOCID: TIntegerField
      FieldName = 'DOCID'
      Origin = 'DOCID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQDocVersionVERSIONID: TIntegerField
      FieldName = 'VERSIONID'
      Origin = 'VERSIONID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQDocVersionFILEPATH: TStringField
      FieldName = 'FILEPATH'
      Origin = 'FILEPATH'
      Required = True
      Size = 256
    end
    object FDQDocVersionFILENAME: TStringField
      FieldName = 'FILENAME'
      Origin = 'FILENAME'
      Required = True
      Size = 256
    end
    object FDQDocVersionDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQDocVersionCREATEDBY: TStringField
      FieldName = 'CREATEDBY'
      Origin = 'CREATEDBY'
      Required = True
      Size = 10
    end
    object FDQDocVersionCREATEDDATE: TSQLTimeStampField
      FieldName = 'CREATEDDATE'
      Origin = 'CREATEDDATE'
      Required = True
    end
  end
  object FDQGetMaxVersionID: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    UpdateTransaction = FDTransUpdate
    SQL.Strings = (
      'select MAX(d.versionid) as MAXID'
      'from documentversion d'
      'where d.docid = :DOCID')
    Left = 64
    Top = 320
    ParamData = <
      item
        Name = 'DOCID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDQGetMaxVersionIDMAXID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'MAXID'
      Origin = 'MAXID'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object FDTransUpdate: TFDTransaction
    Connection = dmDbConnection.FDConnection1
    Left = 64
    Top = 168
  end
  object FDQGetDocByRelation: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    SQL.Strings = (
      'select * from docrelation r'
      'left join documents v'
      'on r.docid = v.docid'
      'where r.reltableid = :RELTABLEID'
      'and r.reltablelineid = :RELTABLELINEID')
    Left = 64
    Top = 376
    ParamData = <
      item
        Name = 'RELTABLEID'
        DataType = ftWideString
        ParamType = ptInput
        Value = '5'
      end
      item
        Name = 'RELTABLELINEID'
        DataType = ftInteger
        ParamType = ptInput
        Value = 0
      end>
  end
  object FDQGlobalDocList: TFDQuery
    Connection = dmDbConnection.FDConnection1
    Transaction = FDTransDocuments
    SQL.Strings = (
      
        'select distinct(D.docid), d.description, d.dataareaid, r.reltabl' +
        'eid, r.reltablelineid'
      'from documents D'
      'left join docrelation r'
      'on d.docid = r.docid'
      'where (r.reltableid = :RELTABLEID'
      'and r.reltablelineid = :RELTABLELINEID'
      'AND D.dataareaid = :DATAAREAID)'
      'or :SHOWALL = 1')
    Left = 64
    Top = 62
    ParamData = <
      item
        Name = 'RELTABLEID'
        DataType = ftSmallint
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'RELTABLELINEID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
        ParamType = ptInput
      end
      item
        Name = 'SHOWALL'
        DataType = ftInteger
        ParamType = ptInput
        Value = 1
      end>
    object FDQGlobalDocListDOCID: TIntegerField
      FieldName = 'DOCID'
      Origin = 'DOCID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQGlobalDocListDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
      Size = 100
    end
    object FDQGlobalDocListDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQGlobalDocListRELTABLEID: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'RELTABLEID'
      Origin = 'RELTABLEID'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDQGlobalDocListRELTABLELINEID: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'RELTABLELINEID'
      Origin = 'RELTABLELINEID'
      ProviderFlags = []
      ReadOnly = True
    end
    object FDQGlobalDocListTABLENAME: TStringField
      FieldKind = fkLookup
      FieldName = 'TABLENAME'
      LookupDataSet = dmSystemTables.CDSRefTables
      LookupKeyFields = 'ID'
      LookupResultField = 'TableName'
      KeyFields = 'RELTABLEID'
      Size = 50
      Lookup = True
    end
    object FDQGlobalDocListMASTERFORMNAME: TStringField
      FieldKind = fkLookup
      FieldName = 'MASTERFORMNAME'
      LookupDataSet = dmSystemTables.CDSRefTables
      LookupKeyFields = 'ID'
      LookupResultField = 'MasterFormName'
      KeyFields = 'RELTABLEID'
      Size = 50
      Lookup = True
    end
  end
  object FDMemGlobalDocList: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'DOCID'
        DataType = ftInteger
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'DATAAREAID'
        DataType = ftSmallint
      end
      item
        Name = 'RELTABLEID'
        DataType = ftInteger
      end
      item
        Name = 'RELTABLELINEID'
        DataType = ftInteger
      end
      item
        Name = 'TABLENAME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MASTERFORMNAME'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 176
    Top = 62
    object FDMemGlobalDocListDOCID: TIntegerField
      FieldName = 'DOCID'
    end
    object FDMemGlobalDocListDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 100
    end
    object FDMemGlobalDocListDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
    end
    object FDMemGlobalDocListRELTABLEID: TIntegerField
      FieldName = 'RELTABLEID'
    end
    object FDMemGlobalDocListRELTABLELINEID: TIntegerField
      FieldName = 'RELTABLELINEID'
    end
    object FDMemGlobalDocListTABLENAME: TStringField
      FieldName = 'TABLENAME'
      Size = 50
    end
    object FDMemGlobalDocListMASTERFORMNAME: TStringField
      FieldName = 'MASTERFORMNAME'
      Size = 50
    end
  end
end
