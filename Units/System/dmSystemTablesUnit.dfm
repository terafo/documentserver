object dmSystemTables: TdmSystemTables
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 384
  Width = 563
  object FDQUserName: TFDQuery
    ActiveStoredUsage = [auDesignTime]
    Connection = dmDbConnection.FDConnection1
    SQL.Strings = (
      'select IDNR, NAVN, SURNAME, RATTINDI, LOYNIORD, DATAAREAID,'
      'language'
      'from aarbeidsfolk'
      'WHERE IDNR = :IDNR')
    Left = 64
    Top = 48
    ParamData = <
      item
        Name = 'IDNR'
        DataType = ftWideString
        ParamType = ptInput
        Value = '001'
      end>
    object FDQUserNameIDNR: TStringField
      FieldName = 'IDNR'
      Origin = 'IDNR'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 10
    end
    object FDQUserNameNAVN: TStringField
      FieldName = 'NAVN'
      Origin = 'NAVN'
      Size = 30
    end
    object FDQUserNameSURNAME: TStringField
      FieldName = 'SURNAME'
      Origin = 'SURNAME'
      Size = 50
    end
    object FDQUserNameRATTINDI: TSmallintField
      FieldName = 'RATTINDI'
      Origin = 'RATTINDI'
    end
    object FDQUserNameLOYNIORD: TStringField
      FieldName = 'LOYNIORD'
      Origin = 'LOYNIORD'
    end
    object FDQUserNameDATAAREAID: TSmallintField
      FieldName = 'DATAAREAID'
      Origin = 'DATAAREAID'
      Required = True
    end
    object FDQUserNameLANGUAGE: TStringField
      FieldName = 'LANGUAGE'
      Origin = 'LANGUAGE'
      Required = True
      FixedChar = True
      Size = 2
    end
  end
  object CDSRefTables: TClientDataSet
    PersistDataPacket.Data = {
      490100009619E0BD01000000180000000A000000000003000000490102494404
      00010000000000095461626C654E616D65010049000000010005574944544802
      0002003200134D6173746572466F726D436C6173734E616D6501004900000001
      000557494454480200020032000B49444669656C644E616D6501004900000001
      000557494454480200020020000B49444669656C645479706501004900000001
      00055749445448020002001400094E616D654669656C64010049000000010005
      57494454480200020064000D4D61737465725461626C65494404000100000000
      00134D61737465725461626C654B65794669656C640100490000000100055749
      445448020002003C000E4D6173746572466F726D4E616D650100490000000100
      0557494454480200020032000A446174614D6F64756C65010049000000010005
      57494454480200020032000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'TableName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MasterFormClassName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'IDFieldName'
        DataType = ftString
        Size = 32
      end
      item
        Name = 'IDFieldType'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NameField'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'MasterTableID'
        DataType = ftInteger
      end
      item
        Name = 'MasterTableKeyField'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'MasterFormName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'DataModule'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <
      item
        Name = 'ID_IDX'
        Fields = 'ID'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'TABLENAME_IDX'
        Fields = 'TableName'
        Options = [ixUnique]
      end>
    IndexName = 'ID_IDX'
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 101
    object CDSRefTablesID: TIntegerField
      FieldName = 'ID'
    end
    object CDSRefTablesTableName: TStringField
      FieldName = 'TableName'
      Size = 50
    end
    object CDSRefTablesMasterFormClassName: TStringField
      FieldName = 'MasterFormClassName'
      Size = 50
    end
    object CDSRefTablesMasterFormName: TStringField
      FieldName = 'MasterFormName'
      Size = 50
    end
    object CDSRefTablesIDFieldName: TStringField
      FieldName = 'IDFieldName'
      Size = 32
    end
    object CDSRefTablesIDFieldType: TStringField
      FieldName = 'IDFieldType'
    end
    object CDSRefTablesNameField: TStringField
      FieldName = 'NameField'
      Size = 100
    end
    object CDSRefTablesMasterTableID: TIntegerField
      FieldName = 'MasterTableID'
    end
    object CDSRefTablesMasterTableKeyField: TStringField
      FieldName = 'MasterTableKeyField'
      Size = 60
    end
    object CDSRefTablesDataModule: TStringField
      FieldName = 'DataModule'
      Size = 50
    end
  end
  object DSoRefTables: TDataSource
    DataSet = CDSRefTables
    Left = 142
    Top = 101
  end
  object CdsTableFieldMasterTable: TClientDataSet
    PersistDataPacket.Data = {
      BE0000009619E0BD010000001800000005000000000003000000BE0009546162
      6C654E616D650100490000000100055749445448020002003200094669656C64
      4E616D650100490000000100055749445448020002003200094D61696E546162
      6C650100490000000100055749445448020002003200124D61696E5461626C65
      4669656C644E616D6501004900000001000557494454480200020032000E4D61
      696E5461626C6552656649440100490000000100055749445448020002003200
      0000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'TableName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'FieldName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MainTable'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MainTableFieldName'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'MainTableRefID'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <
      item
        Name = 'CdsTableFieldMasterTableIndex1'
        Fields = 'MainTable;FieldName'
        Options = [ixPrimary, ixUnique]
      end>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 167
    object CdsTableFieldMasterTableTableName: TStringField
      FieldName = 'TableName'
      Size = 50
    end
    object CdsTableFieldMasterTableFieldName: TStringField
      FieldName = 'FieldName'
      Size = 50
    end
    object CdsTableFieldMasterTableMainTable: TStringField
      FieldName = 'MainTable'
      Size = 50
    end
    object CdsTableFieldMasterTableMainTableFieldName: TStringField
      FieldName = 'MainTableFieldName'
      Size = 50
    end
    object CdsTableFieldMasterTableMainTableRefID: TStringField
      FieldName = 'MainTableRefID'
      Size = 50
    end
  end
end
