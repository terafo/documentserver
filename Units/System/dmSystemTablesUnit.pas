unit dmSystemTablesUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

const
    TABLE_CUSTOMERS = 7;   // Kundar
    TABLE_CONTACTS = 29;

type
  TRefTableID = record
    Aarbeidsfolk,
    Associaton,
    AssociationPayments,
    AssocPaymentInventory,
    Bon,
    BonLines,
    Kundar,
    LoginTable,
    MixCampaigns,
    MixCampaignLines,
    Project,
    Re_TimeSheet_LoginLinesTable,
    SalaryAgreeTable,
    SalaryBonusGroup,
    SalaryBonusLines,
    SalaryList,
    SalaryListLines,
    ShiftTable,
    SummaryInvoice,
    SummaryInvoiceLines,
    TimeSheetTable,
    TimeSheetLines,
    TimeSheet_ExtraPayments,
    VaruNr,
    Vendor,
    WorkAgreementTable: integer;
  end;


  TdmSystemTables = class(TDataModule)
    FDQUserName: TFDQuery;
    FDQUserNameIDNR: TStringField;
    FDQUserNameNAVN: TStringField;
    FDQUserNameSURNAME: TStringField;
    FDQUserNameRATTINDI: TSmallintField;
    FDQUserNameLOYNIORD: TStringField;
    FDQUserNameDATAAREAID: TSmallintField;
    FDQUserNameLANGUAGE: TStringField;
    CDSRefTables: TClientDataSet;
    CDSRefTablesID: TIntegerField;
    CDSRefTablesTableName: TStringField;
    CDSRefTablesMasterFormClassName: TStringField;
    CDSRefTablesMasterFormName: TStringField;
    CDSRefTablesIDFieldName: TStringField;
    CDSRefTablesIDFieldType: TStringField;
    CDSRefTablesNameField: TStringField;
    CDSRefTablesMasterTableID: TIntegerField;
    CDSRefTablesMasterTableKeyField: TStringField;
    CDSRefTablesDataModule: TStringField;
    DSoRefTables: TDataSource;
    CdsTableFieldMasterTable: TClientDataSet;
    CdsTableFieldMasterTableTableName: TStringField;
    CdsTableFieldMasterTableFieldName: TStringField;
    CdsTableFieldMasterTableMainTable: TStringField;
    CdsTableFieldMasterTableMainTableFieldName: TStringField;
    CdsTableFieldMasterTableMainTableRefID: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure SetRefTableID;
    procedure RegTableAppend(TableID, MasterTableID: integer; TableName, FormClassName, FormName, DMClassName, IDFieldName, IDFieldType, DescrField: string);
    procedure FillTableFieldMasterTable;
    procedure RegisterMasterTables(ActiveTable, FocusedField, MasterTableName, MasterTableField: string);
  public
    { Public declarations }
    RefTableID: TRefTableID;
    procedure RegisterTables;
  end;

var
  dmSystemTables: TdmSystemTables;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}



procedure TdmSystemTables.DataModuleCreate(Sender: TObject);
begin
    RegisterTables;
    SetRefTableID;
    FillTableFieldMasterTable;
end;

procedure TdmSystemTables.FillTableFieldMasterTable;
begin
    CdsTableFieldMasterTable.EmptyDataSet;

    // Login Corrections
    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='QARBEIDSLINJUR';
    CdsTableFieldMasterTableFieldName.Value:='AARFONR';
    CdsTableFieldMasterTableMainTable.Value:='AARBEIDSFOLK';
    CdsTableFieldMasterTableMainTableFieldName.Value:='IDNR';
    CdsTableFieldMasterTable.Post;

    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='QARBEIDSLINJUR';
    CdsTableFieldMasterTableFieldName.Value:='CNYBYNR';
    CdsTableFieldMasterTableMainTable.Value:='CNYBYGNINGSARBEIDI';
    CdsTableFieldMasterTableMainTableFieldName.Value:='NR';
    CdsTableFieldMasterTable.Post;

    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='QARBEIDSLINJUR';
    CdsTableFieldMasterTableFieldName.Value:='SHIFTWORKID';
    CdsTableFieldMasterTableMainTable.Value:='SHIFTWORK';
    CdsTableFieldMasterTableMainTableFieldName.Value:='ID';
    CdsTableFieldMasterTable.Post;

    // ASSOCIATIONPAYMENT_INVENTORY
    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='TIMAKLADDA';
    CdsTableFieldMasterTableFieldName.Value:='STARVSFOLK';
    CdsTableFieldMasterTableMainTable.Value:='AARBEIDSFOLK';
    CdsTableFieldMasterTableMainTableFieldName.Value:='IDNR';
    CdsTableFieldMasterTable.Post;

    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='TIMESHEET_EXTRAPAYMENTS';
    CdsTableFieldMasterTableFieldName.Value:='TIMESHEETNO';
    CdsTableFieldMasterTableMainTable.Value:='TIMAKLADDA';
    CdsTableFieldMasterTableMainTableFieldName.Value:='KLADDANR';
    CdsTableFieldMasterTable.Post;

    //TIMABOKINGAR
    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='TIMABOKINGAR';
    CdsTableFieldMasterTableFieldName.Value:='REFLINJUNR';
    CdsTableFieldMasterTableMainTableRefID.Value:='REFTABEL';
    CdsTableFieldMasterTableMainTable.Value:='TIMAKLADDALINJUR';
    CdsTableFieldMasterTableMainTableFieldName.Value:='LINJUNR';
    CdsTableFieldMasterTable.Post;

    // General Ledger
    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='RE_GENLEDGER_ORIGINAL';
    CdsTableFieldMasterTableFieldName.Value:='REFTABLELINEID';
    CdsTableFieldMasterTableMainTableRefID.Value:='REFTABEL';
    CdsTableFieldMasterTableMainTable.Value:='BONLINJUR';
    CdsTableFieldMasterTableMainTableFieldName.Value:='LINJUID';
    CdsTableFieldMasterTable.Post;

    // Bonyvirlit
    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:='BONLINJUR';
    CdsTableFieldMasterTableFieldName.Value:='VARUNRID';
    //CdsTableFieldMasterTableMainTableRefID.Value:='REFTABEL';
    CdsTableFieldMasterTableMainTable.Value:='VARUNR';
    CdsTableFieldMasterTableMainTableFieldName.Value:='VARUNRID';
    CdsTableFieldMasterTable.Post;


    RegisterMasterTables('AARBEIDSFOLK','WORKINGAGREEMENT','AGREEDWORKHOURS','ID');   //FormEmployees
    RegisterMasterTables('AARBEIDSFOLK','IDNR','AARBEIDSFOLK','IDNR');
    RegisterMasterTables('AGREEDWORKHOURS','SALARYAGREEMENT','LONARFLOKKAR','NR');   // WorkingAgreements
    RegisterMasterTables('QARBEIDSLINJUR','NR','QARBEIDSLINJUR','NR');
    //RegisterMasterTables('
    RegisterMasterTables('TIMESHEET_EXTRAPAYMENTS','ASSOCIATIONPAYMENTID','ASSOCIATIONPAYMENTS','ID');
    RegisterMasterTables('TIMESHEET_EXTRAPAYMENTS','ASSOCIATPAYMINVENTORYID','ASSOCIATIONPAYMENT_INVENTORY','ID');
end;

procedure TdmSystemTables.RegisterMasterTables(ActiveTable, FocusedField,
  MasterTableName, MasterTableField: string);
begin
    CdsTableFieldMasterTable.Insert;
    CdsTableFieldMasterTableTableName.Value:=ActiveTable;
    CdsTableFieldMasterTableFieldName.Value:=FocusedField;
    CdsTableFieldMasterTableMainTable.Value:=MasterTableName;
    CdsTableFieldMasterTableMainTableFieldName.Value:=MasterTableField;
    CdsTableFieldMasterTable.Post;
end;

procedure TdmSystemTables.RegisterTables;
begin
    CDSRefTables.EmptyDataSet;

    //CDSRefTablesMasterFormName is used with the Exportfilter function
    //CDSRefTablesDataModule is used with the Exportfilter function
    //CDSRefTablesMasterTableID: If CDSRefTablesID is a childtable, MasterTableID is the ref to the mastertable
    CDSRefTables.append;
    CDSRefTablesID.Value:=5;
    CDSRefTablesMasterTableID.Value:=5;
    CDSRefTablesTableName.Value:='AARBEIDSFOLK';
    CDSRefTablesMasterFormClassName.Value:='TFormEmployee';
    CDSRefTablesMasterFormName.Value:='FormEmployee';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='IDNR';
    CDSRefTablesIDFieldType.Value:='VARCHAR';
    CDSRefTablesNameField.Value:='NAVN';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=11;
    CDSRefTablesMasterTableID.Value:=11;
    CDSRefTablesTableName.Value:='AGREEDWORKHOURS';    //Working Agreements
    CDSRefTablesMasterFormClassName.Value:='TFormWorkingAgreements';
    CDSRefTablesMasterFormName.Value:='FormWorkingAgreements';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='ID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='DESCRIPTION';
    CDSRefTables.Post;



    CDSRefTables.append;
    CDSRefTablesID.Value:=4;
    CDSRefTablesMasterTableID.Value:=4;
    CDSRefTablesTableName.Value:='ASSOCIATION';
    CDSRefTablesMasterFormClassName.Value:='TFormAssociation';
    CDSRefTablesMasterFormName.Value:='FormAssociation';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='ID';
    CDSRefTablesIDFieldType.Value:='VARCHAR';
    CDSRefTablesNameField.Value:='NAVN';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=19;
    CDSRefTablesMasterTableID.Value:=4;
    CDSRefTablesTableName.Value:='ASSOCIATIONPAYMENTS';
    CDSRefTablesMasterFormClassName.Value:='TFormAssociation';
    CDSRefTablesMasterFormName.Value:='FormAssociation';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='ID';
    CDSRefTablesIDFieldType.Value:='VARCHAR';
    CDSRefTablesNameField.Value:='DESCRIPTION';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=6;
    CDSRefTablesMasterTableID.Value:=6;
    CDSRefTablesTableName.Value:='ASSOCIATIONPAYMENT_INVENTORY';
    CDSRefTablesMasterFormClassName.Value:='TFormAssociationPayments';
    CDSRefTablesMasterFormName.Value:='FormAssociationPayments';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='ID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=9;
    CDSRefTablesMasterTableID.Value:=9;
    CDSRefTablesTableName.Value:='BON';
    CDSRefTablesMasterFormClassName.Value:='TBonYvirlitForm';
    CDSRefTablesMasterFormName.Value:='BonYvirlitForm';
    CDSRefTablesDataModule.Value:='';
    //CDSRefTablesIDFieldName.Value:='BONNR;BONDATOTID;KASSANR';
    CDSRefTablesIDFieldName.Value:='BONNR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;



    CDSRefTables.append;
    CDSRefTablesID.Value:=2;
    CDSRefTablesMasterTableID.Value:=9;
    CDSRefTablesTableName.Value:='BONLINJUR';
    CDSRefTablesMasterTableKeyField.Value:='BONNR,BONDATOTID,KASSANR';
    CDSRefTablesMasterFormClassName.Value:='TBonYvirlitForm';
    CDSRefTablesMasterFormName.Value:='BonYvirlitForm';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='LINJUID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;

    // ProjectTable
    RegTableAppend(15,15,'CNYBYGNINGSARBEIDI','TProjectForm','ProjectForm:','TdmProject','NR','INTEGER','TEKST');
    RegTableAppend(8,8,'FAKTURAUTHOVD','TFakturaUtForm','FakturaUtForm','','FAKTURAUTID','INTEGER','NAME');

    CDSRefTables.append;
    CDSRefTablesID.Value:=1;
    CDSRefTablesMasterTableID.Value:=8;
    CDSRefTablesTableName.Value:='FAKTURAUTLINJUR';
    CDSRefTablesMasterTableKeyField.Value:='FAKTURAHOVDUTID';
    CDSRefTablesMasterFormClassName.Value:='TFakturaUtForm';
    CDSRefTablesMasterFormName.Value:='';  // FakturaUtForm is declared twice
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='FAKTURAUTLINJUNR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=7;
    CDSRefTablesMasterTableID.Value:=7;
    CDSRefTablesTableName.Value:='KUNDAR';
    CDSRefTablesMasterFormClassName.Value:='TKundaForm';
    CDSRefTablesMasterFormName.Value:='KundaForm';
    CDSRefTablesIDFieldName.Value:='NR';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='NAVN';
    CDSRefTables.Post;


    CDSRefTables.append;
    CDSRefTablesID.Value:=3;
    CDSRefTablesMasterTableID.Value:=3;
    CDSRefTablesTableName.Value:='LEVERANDOR';
    CDSRefTablesMasterFormClassName.Value:='TLeverandorForm';
    CDSRefTablesMasterFormName.Value:='LeverandorForm';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='NR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='FORNAVN';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=12;
    CDSRefTablesMasterTableID.Value:=12;
    CDSRefTablesTableName.Value:='LONARFLOKKAR';
    CDSRefTablesMasterFormClassName.Value:='TLonarflokkarForm';
    CDSRefTablesMasterFormName.Value:='LonarflokkarForm';
    CDSRefTablesDataModule.Value:='';
    CDSRefTablesIDFieldName.Value:='NR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='NAVN';
    CDSRefTables.Post;

    //Lønarlisti: Salarylist
    RegTableAppend(23,23,'LONARLISTI','TFormLonarListi','FormLonarListi','TdmSalaryList','ID','integer','');
    RegTableAppend(24,23,'LONARLISTILINJUR','TFormLonarListi','FormLonarListi','TdmSalaryList','LINJUID','integer','');

    // Mixrabathovd: MixCampaigns
    RegTableAppend(25,25,'MIXRABATHOVD','TMixRabattirForm','MixRabattirForm:','','TILBODNR','integer','');
    RegTableAppend(26,25,'MIXRABATLINJUR','TMixRabattirForm','MixRabattirForm:','','LINJUID','integer','');


    CDSRefTables.append;
    CDSRefTablesID.Value:=13;
    CDSRefTablesMasterTableID.Value:=13;
    CDSRefTablesTableName.Value:='QARBEIDSLINJUR';
    CDSRefTablesMasterFormClassName.Value:='TFormStemplingar';
    CDSRefTablesMasterFormName.Value:='FormStemplingar';
    CDSRefTablesDataModule.Value:='TdmLoginCorrection';
    CDSRefTablesIDFieldName.Value:='NR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=17;
    CDSRefTablesMasterTableID.Value:=17;
    CDSRefTablesTableName.Value:='RE_QARBLIN_TIMAKLINJNR';
    CDSRefTablesMasterFormClassName.Value:=''; //'TTimaSedilForm';
    CDSRefTablesMasterFormName.Value:=''; //'TimaSedilForm';
    CDSRefTablesDataModule.Value:=''; //'TdmTimeSheet';
    CDSRefTablesIDFieldName.Value:='LINEID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=18;
    CDSRefTablesMasterTableID.Value:=18;
    CDSRefTablesTableName.Value:='SALARYBONUS';
    CDSRefTablesMasterFormClassName.Value:='TFormSalaryBonus';
    CDSRefTablesMasterFormName.Value:='FormSalaryBonus';
    CDSRefTablesDataModule.Value:=''; //'TdmTimeSheet';
    CDSRefTablesIDFieldName.Value:='BONUSID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='NAME';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=22;
    CDSRefTablesMasterTableID.Value:=18;
    CDSRefTablesTableName.Value:='SALARYBONUSGROUP';
    CDSRefTablesMasterFormClassName.Value:='TFormSalaryBonus';
    CDSRefTablesMasterFormName.Value:='FormSalaryBonus';
    CDSRefTablesDataModule.Value:=''; //'TdmTimeSheet';
    CDSRefTablesIDFieldName.Value:='BONUSGROUPID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='DESCRIPTION';
    CDSRefTables.Post;



    // Shifttable
    CDSRefTables.append;
    CDSRefTablesID.Value:=14;
    CDSRefTablesMasterTableID.Value:=14;
    CDSRefTablesTableName.Value:='SHIFTWORK';
    CDSRefTablesMasterFormClassName.Value:='TFormShifts';
    CDSRefTablesMasterFormName.Value:='FormShifts';
    CDSRefTablesDataModule.Value:='TdmProject';
    CDSRefTablesIDFieldName.Value:='ID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='';
    CDSRefTables.Post;

    RegTableAppend(27,27,'SUMMARYINVOICE','TFormSummaryInvoice','FormSummaryInvoice','TdmSummaryInvoice','ID','INTEGER','');
    RegTableAppend(28,27,'SUMMARYINVOICELINE','TFormSummaryInvoice','FormSummaryInvoice','TdmSummaryInvoice','ID','INTEGER','');
    RegTableAppend(TABLE_CONTACTS,TABLE_CUSTOMERS,'CONTACTS','TKundaForm','KundaForm',' ','ID','INTEGER','');


    CDSRefTables.append;
    CDSRefTablesID.Value:=16;
    CDSRefTablesMasterTableID.Value:=16;
    CDSRefTablesTableName.Value:='TIMAKLADDA';
    CDSRefTablesMasterFormClassName.Value:='TTimaSedilForm';
    CDSRefTablesMasterFormName.Value:='TimaSedilForm';
    CDSRefTablesDataModule.Value:='TdmTimeSheet';
    CDSRefTablesIDFieldName.Value:='KLADDANR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='TEKSTUR';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=21;
    CDSRefTablesMasterTableID.Value:=16;
    CDSRefTablesTableName.Value:='TIMAKLADDALINJUR';
    CDSRefTablesMasterFormClassName.Value:='TTimaSedilForm';
    CDSRefTablesMasterFormName.Value:='TimaSedilForm';
    CDSRefTablesDataModule.Value:='TdmTimeSheet';
    CDSRefTablesIDFieldName.Value:='LINJUNR';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='TEKSTUR';
    CDSRefTables.Post;

    CDSRefTables.append;
    CDSRefTablesID.Value:=20;
    CDSRefTablesMasterTableID.Value:=16;
    CDSRefTablesTableName.Value:='TIMESHEET_EXTRAPAYMENTS';
    CDSRefTablesMasterFormClassName.Value:='TTimaSedilForm';
    CDSRefTablesMasterFormName.Value:='TimaSedilForm';
    CDSRefTablesDataModule.Value:='TdmTimeSheet';
    CDSRefTablesIDFieldName.Value:='ID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='DESCRIPTION';
    CDSRefTables.Post;


    CDSRefTables.append;
    CDSRefTablesID.Value:=10;
    CDSRefTablesMasterTableID.Value:=10;
    CDSRefTablesTableName.Value:='VARUNR';
    CDSRefTablesMasterFormClassName.Value:='TVaruForm';
    CDSRefTablesMasterFormName.Value:='VaruForm';
    CDSRefTablesDataModule.Value:='';
    //CDSRefTablesIDFieldName.Value:='BONNR;BONDATOTID;KASSANR';
    CDSRefTablesIDFieldName.Value:='VARUNRID';
    CDSRefTablesIDFieldType.Value:='INTEGER';
    CDSRefTablesNameField.Value:='VARUNAVN';
    CDSRefTables.Post;


    // RegisterClass in MainForm OnCreate
end;

procedure TdmSystemTables.RegTableAppend(TableID, MasterTableID: integer;
  TableName, FormClassName, FormName, DMClassName, IDFieldName, IDFieldType,
  DescrField: string);
begin
    CDSRefTables.append;
    CDSRefTablesID.Value:=TableID;
    CDSRefTablesMasterTableID.Value:=MasterTableID;
    CDSRefTablesTableName.Value:=TableName;
    CDSRefTablesMasterFormClassName.Value:=FormClassName;
    CDSRefTablesMasterFormName.Value:=FormName;
    CDSRefTablesDataModule.Value:=DMClassName;
    CDSRefTablesIDFieldName.Value:=IDFieldName;
    CDSRefTablesIDFieldType.Value:=IDFieldType;
    CDSRefTablesNameField.Value:=DescrField;
    CDSRefTables.Post;
end;

procedure TdmSystemTables.SetRefTableID;
begin
     // ....RefID values are used in GoToMainTable function
  if CDSRefTables.Locate('TableName', 'AARBEIDSFOLK', []) then
    RefTableID.Aarbeidsfolk := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'AGREEDWORKHOURS', []) then
    RefTableID.WorkAgreementTable := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'ASSOCIATION', []) then
    RefTableID.Associaton := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'ASSOCIATIONPAYMENTS', []) then
    RefTableID.AssociationPayments := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'ASSOCIATIONPAYMENT_INVENTORY', []) then
    RefTableID.AssocPaymentInventory := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'BON', []) then
    RefTableID.Bon := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'BONLINJUR', []) then
    RefTableID.Bonlines := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TableName', 'CNYBYGNINGSARBEIDI', []) then
    RefTableID.Project:=CDSRefTablesID.Value
  else if CDSRefTables.Locate('TableName', 'KUNDAR', []) then
    RefTableID.Kundar := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'LONARFLOKKAR', []) then
    RefTableID.SalaryAgreeTable := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'LONARLISTI', []) then
    RefTableID.SalaryList := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'LONARLISTILINJUR', []) then
    RefTableID.SalaryListLines := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'LEVERANDOR', []) then
    RefTableID.Vendor := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'MIXRABATHOVD', []) then
    RefTableID.MixCampaigns := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'MIXRABATLINJUR', []) then
    RefTableID.MixCampaigns := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'QARBEIDSLINJUR', []) then
    RefTableID.LoginTable := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'RE_QARBLIN_TIMAKLINJNR', []) then
    RefTableID.Re_TimeSheet_LoginLinesTable := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'SALARYBONUS', []) then
    RefTableID.SalaryBonusLines := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'SALARYBONUSGROUP', []) then
    RefTableID.SalaryBonusGroup := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'SHIFTWORK', []) then
    RefTableID.ShiftTable := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'SUMMARYINVOICE', []) then
    RefTableID.SummaryInvoice:=CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'SUMMARYINVOICELINE', []) then
    RefTableID.SummaryInvoiceLines:=CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'TIMAKLADDA', []) then
    RefTableID.TimeSheetTable := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'TIMAKLADDALINJUR', []) then
    RefTableID.TimeSheetLines := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'TIMESHEET_EXTRAPAYMENTS', []) then
    RefTableID.TimeSheet_ExtraPayments := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'VARUNR', []) then
    RefTableID.VaruNr := CDSRefTablesID.Value
  else if CDSRefTables.Locate('TABLENAME', 'CUSTDELIVERYADDRES', []) then
    RefTableID.VaruNr := CDSRefTablesID.Value;
end;


end.
