unit dmDbConnectionUnit;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  FireDAC.Phys.IBDef, FireDAC.Phys.IBBase, FireDAC.Phys.IB, FireDAC.Comp.Client,
  Data.DB, FireDAC.VCLUI.Wait, System.IOUtils, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TSettings = record
    Server: string;
    DataBase: string;
    RootFolder: string;
  end;


  TdmDbConnection = class(TDataModule)
    FDConnection1: TFDConnection;
    FDTransaction1: TFDTransaction;
    FDPhysIBDriverLink1: TFDPhysIBDriverLink;
    FDQGetGenID: TFDQuery;
    FDTransGenID: TFDTransaction;
    procedure DataModuleCreate(Sender: TObject);
    procedure FDConnection1BeforeConnect(Sender: TObject);
  private
    { Private declarations }
  public
    Settings: TSettings;
    function  GetGenID (GeneratorNavn : string) : integer;
  end;

var
  dmDbConnection: TdmDbConnection;

implementation

uses
  FMX.Forms, FMX.Dialogs;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdmDbConnection.DataModuleCreate(Sender: TObject);
var
    IniFile: TStringList;
begin
    Inifile:=TStringList.Create;
    try
        IniFile.LoadFromFile(IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)))+'DocumentServer.ini');
        Settings.Server:=IniFile.Values['Server'];
        Settings.DataBase:=IniFile.Values['Database'];
        Settings.RootFolder:=IniFile.Values['DocumentRootFolder'];
    finally
        IniFile.Free;
    end;
    //Settings.DataBase:=
    //Settings.RootFolder:=
    FDConnection1.Connected:=True;
end;

function TdmDbConnection.GetGenID (GeneratorNavn : string) : integer;
begin

   with FDQGetGenID do
   begin
      SQL.Clear;
      SQL.ADD('SELECT GEN_ID ('+GeneratorNavn+', 1) from rdb$database');
      Open;

      // Fields 0 er fyrsta felt � Queryini.
      Result := Fields[0].AsInteger;

      if FDTransGenID.Active then
         FDTransGenID.commit;
   end;
end;

procedure TdmDbConnection.FDConnection1BeforeConnect(Sender: TObject);
begin
    if (Settings.DataBase > ' ') then
    begin
        //FDConnection1.Params.DriverID:='IB';
        FDConnection1.Params.Add('Server=:'+Settings.Server);
        FDConnection1.Params.Database:=Settings.DataBase;
    end;
end;

end.
