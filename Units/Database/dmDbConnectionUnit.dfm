object dmDbConnection: TdmDbConnection
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 266
  Width = 476
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\TestDB\Forritan\Projectstyring\PROJECTSTYRING_Dev.IB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Protocol=TCPIP'
      'Server=127.0.0.1'
      'DriverID=IB')
    ConnectedStoredUsage = []
    LoginPrompt = False
    Transaction = FDTransaction1
    BeforeConnect = FDConnection1BeforeConnect
    Left = 64
    Top = 40
  end
  object FDTransaction1: TFDTransaction
    Connection = FDConnection1
    Left = 160
    Top = 40
  end
  object FDPhysIBDriverLink1: TFDPhysIBDriverLink
    Left = 272
    Top = 48
  end
  object FDQGetGenID: TFDQuery
    Connection = FDConnection1
    Transaction = FDTransGenID
    Left = 64
    Top = 88
  end
  object FDTransGenID: TFDTransaction
    Connection = FDConnection1
    Left = 152
    Top = 88
  end
end
