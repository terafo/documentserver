unit ServerMethodsUnit1;

interface

uses System.SysUtils, System.Classes, System.Json,
    Datasnap.DSServer, Datasnap.DSAuth, Data.FireDACJSONReflect;

type
{$METHODINFO ON}
  TServerMethods1 = class(TDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    function DocumentList(RefTableID, DataAreaID: SmallInt; ReftableLineID: integer; EmplID: string; ShowAll: SmallInt): TFDJSONDataSets;
    function GlobalDocList: TFDJSONDataSets;
    function GetDocument(DocID, DocVersion: integer): TFDJSONDataSets;
    function GetDocVersion(DocID: integer): TFDJSONDataSets;
    procedure SaveDocument(const ADeltaList: TFDJSONDEltas);
    procedure SaveDoc(Document: TFDJSONDataSets);
  end;
{$METHODINFO OFF}



implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}


uses System.StrUtils, dmDocumentsUnit, FMX.Dialogs, dmDbConnectionUnit;

const
    sDocument = 'DOCUMENT';

function TServerMethods1.DocumentList(RefTableID, DataAreaID: SmallInt; ReftableLineID: integer; EmplID: string; ShowAll: SmallInt): TFDJSONDataSets;
begin
    Result:=dmDocuments.GetDocumentList(RefTableID, DataAreaID, ReftableLineID, EmplID, ShowAll);
end;

function TServerMethods1.EchoString(Value: string): string;
begin
  Result := Value;
end;


function TServerMethods1.GetDocument(DocID, DocVersion: integer): TFDJSONDataSets;
begin
    result:=dmDocuments.GetDocument(DocID, DocVersion);
end;

function TServerMethods1.GetDocVersion(DocID: integer): TFDJSONDataSets;
begin
    result:=dmDocuments.GetDocVersions(DocID);
end;

function TServerMethods1.GlobalDocList: TFDJSONDataSets;
begin
    result:=dmDocuments.GetGlobalDocList;
end;

function TServerMethods1.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;


procedure TServerMethods1.SaveDoc(Document: TFDJSONDataSets);
begin
    dmDocuments.FDMemDocument.EmptyDataSet;
    dmDocuments.FDMemDocument.Active:=False;
    dmDocuments.FDMemDocument.AppendData(TFDJSONDataSetsReader.GetListValue(Document, 0));

    if (dmDocuments.FDMemDocumentDOCID.Value = -1) then
       dmDocuments.SaveNewDocument
    else
       dmDocuments.UpdateDocument;
end;

procedure TServerMethods1.SaveDocument(const ADeltaList: TFDJSONDEltas);
var
  LApply: IFDJSONDeltasApplyUpdates;
begin
  dmDocuments.FDQSaveDocument.Open;
  // Create the apply object
  LApply := TFDJSONDeltasApplyUpdates.Create(ADeltaList);
  // Apply the department delta
  LApply.ApplyUpdates(sDocument, dmDocuments.FDQSaveDocument.Command);

  if LApply.Errors.Count > 0 then         // Raise an exception if any errors.
      raise Exception.Create(LApply.Errors.Strings.Text);
end;

end.

